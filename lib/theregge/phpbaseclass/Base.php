<?php
/**
 * PHP Base Class
 * A very basic class to avoid boilerplate code.
 * 
 * @version     0.1.1
 * @author      Regis Zaleman <regis.zaleman@gmail.com>
 * @license     MIT
 */

namespace theregge\phpbaseclass;

class Base
{
    /**
     * Array to hold class params;
     * @var array
     */ 
    private $data = array();

    /**
     * Array to hold original class constructor arguments array.
     * @var array | null
     */
    private $constructorArgs;

    /**
     * Array to store errors
     * @var array
     */
    protected $errors = array();

    public function __construct($args=false)
    {
        if ( $args ) {
            $this->constructorArgs = $args;
        }
    }

    /**
     * Magic function setting data
     * @param  string $name data key name
     * @return mixed       null or the data requested
     */
    public function &__get( $name ) {

        if ( array_key_exists($name, $this->data) )
        {
            return $this->data[$name];
        }

        $this->data[$name]  = false;
        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return $this->data[$name];
    }

    /**
     * Magic function getting data
     * @param string $name  data key name
     * @param mixed $value the value to set the data to.
     */
    public function __set( $name, $value ) {
        $this->data[$name] = $value;
    }

    /**
     * As of PHP 5.1.0
     * @param  string  $name The name of the data to check
     * @return boolean
     */
    public function __isset( $name )
    {
        return isset( $this->data[$name] );
    }

    /**
     * As of PHP 5.1.0
     * @param string $name the name of the data to unset
     */
    public function __unset( $name )
    {
        unset( $this->data[$name] );
    }

    /**
     * Check presence and type of a defined set of required parameters.
     *
     * This method can be used in the class constructor to check that all 
     * user defined required parameters are present and of the right data type.
     *         
     * @param  array $params    An array of arrays describing the required parameters, with the keys 'name' and 'type'
     * @return boolean          Returns true if all parameters are present and of the right type. Set the class's 'ready'
     *                          attribute to this value to complete your class initialization.
     * @example
     * // Inside you class's constructor, after calling the parent constructor:
     * $reqParams = array(
     *     array( 'name' => 'myname1', 'type' => 'object' ),
     *     array( 'name' => 'myname2', 'type' => 'integer' ),
     *     array( 'name' => 'myname3', 'type' => 'string')
     * );
     * $this->ready = $this->checkRequiredParams( $reqParams );
     */ 
    public function checkRequiredParams( $params ) {

        if ( 'array' != gettype($params) ) {
            $trace = debug_backtrace();
            trigger_error(
                'requiredParams parameter must be an array via checkRequiredParams,' .
                ' in ' . $trace[0]['file'] .
                ' on line ' . $trace[0]['line'],
                E_USER_NOTICE );
        }

        foreach ($params as $param) {

            if ( 'array' != gettype($param) ||
                ! array_key_exists('name', $param) ||
                ! array_key_exists('type', $param) ) {

                $trace = debug_backtrace();
                trigger_error(
                    'Each required param must be expressed as an array, containing the param name and its data type, ' . 
                    ' in ' . $trace[0]['file'] . 
                    ' on line ' . $trace[0]['line'],
                    E_USER_NOTICE );
                return false;
            }

             $name         = $param['name'];
             $requiredType = $param['type'];
             $type         = gettype($this->constructorArgs[$name]);

             if ( $this->constructorArgs && ! array_key_exists($name, $this->constructorArgs ) ) {
                $this->errors[] = 'Missing required constructor parameter array item ' . $name;
             }
             if ( $requiredType !== $type ) {
                $this->errors[] = 'The class constructor parameter array item ' . $name . ' must be of type: ' . $requiredType . '- type found: ' . $type;
             }
        }

        if ( count( $this->errors ) > 0 ) {
            return false;
        }
        return true;
    }
}