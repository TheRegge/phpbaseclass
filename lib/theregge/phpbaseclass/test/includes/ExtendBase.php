<?php
//require 'lib/theregge/phpbaseclass/Base.php';

use theregge\phpbaseclass\Base;

class ExtendBase extends Base
{

    public function __construct( $args ) {
        
        parent::__construct( $args );

        $reqParams = array(
             array( 'name' => 'myname3', 'type' => 'string')
         );

        $this->ready = $this->checkRequiredParams ( $reqParams );
    }
}