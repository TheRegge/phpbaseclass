<?php
require 'lib/theregge/phpbaseclass/Base.php';
require 'lib/theregge/phpbaseclass/test/includes/ExtendBase.php';
/**
* Test for the base Class
*/

use theregge\phpbaseclass\Base;

class BaseTest extends PHPUnit_Framework_TestCase
{
    
    public function testGetSetIsset() {

        $b = new Base;

        // Set a variable
        $b->param = 10;
        // Get a variable 
        $result = $b->param;
        $this->assertEquals(10, $result);
        $this->assertEquals(true, isset($b->param));
    }

    public function testCheckRequiredParams() {

        $args = array(
            'myname1' => (object) 'mytestObject',
            'myname2' => 4,
            'myname3' => 'Joe',
            'myname4' => true // not required.
        );

        $c = new ExtendBase( $args );

        $this->assertEquals(true, $c->ready);
    }
}